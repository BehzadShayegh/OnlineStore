#include "good.h"

Good::Good(User* _seller){
	seller = _seller;
	cin >> name >> price >> count;
	special_number = 0;
	traded_numtimes = 0;
	saled = false;
	saled_of_market = false;
}
Good::Good(Good* old_good, int new_count){
	seller = old_good->seller;
	name = old_good->name;
	price = old_good->price;
	count = new_count;
	special_number = old_good->special_number;
	traded_numtimes = old_good->traded_numtimes;
	saled = old_good->saled;
	saled_of_market = old_good->saled_of_market;
}

bool Good::same_name(string _name){
	return this->name == _name;
}
bool Good::same_seller(Good* that){
	return (this->seller->same_username(that->seller));
}
int Good::find_good_index(const vector<Good*> goods){
	for(int i=0; i<goods.size(); i++)
		if(goods[i]->same_name(this->name) && goods[i]->use_seller()->same_username(this->seller))
			return i;
	return -1;
}
bool Good::exist_in(vector<Good*> goods){
	return find_good_index(goods)>=0;
}
bool Good::there_is(string _name, int count_need){
	return (this->name == _name && this->count >= count_need);
}
bool Good::smaller_for_show(Good* that){
	return ( ( this->name < that->name)
	||	(this->name == that->name
			&& this->count < that->count)
	||	(this->name == that->name
		&& this->count == that->count
			&& this->price < that->price)
	||	(this->name == that->name
		&& this->count == that->count
		&& this->price == that->price
			&& this->seller->smaller_in_username_than(that->seller))
	);
}
bool Good::enough_count(int count_need){
	return this->count >= count_need;
}
double Good::bought(int count_bought){
	double _price=0;
	calculate_prices(_price,count_bought);

	this->special_number -= count_bought;
		if(this->special_number < 0) this->special_number = 0;
	this->count -= count_bought;
	this->traded_numtimes++;
	return _price;
}
bool Good::is_not_avalable_in_more(){
	return (this->count == 0);
}
User* Good::use_seller(){
	return this->seller;
}
void Good::calculate_prices(double& total_price, int goods_count){
	double _price=0;
	if(saled)
		_price = (this->price)*0.9*goods_count;
	else if(special_number >= goods_count)
		_price = (this->price)*0.9*goods_count;
	else
		_price = (this->price)*0.9*special_number + (this->price)*(goods_count-special_number);
	
	total_price += _price;
}
bool Good::requested_for_sale(string requester){
	for(int i=0; i<sale_requesters.size(); i++)
		if(requester == sale_requesters[i])
			return true;
	return false;
}
void Good::add_to_sale_requesters(string requester){
	sale_requesters.push_back(requester);
}
void Good::check_saled_for_requests(){
	if(sale_requesters.size()>=10)
		saled = true;
	else saled = false;
}
bool Good::bestseller(){
	if(traded_numtimes >= 10)
		return true;
	else return false;
}
void Good::make_special(int _special_number){
	special_number += _special_number;
}
void Good::delete_special(int de_special_number){
	special_number -= de_special_number;
}
int Good::special_count(){
	return special_number;
}
int Good::count_of_this_good_in(vector<Good*> goods){
	int number = 0;
	for(int i=0; i<goods.size(); i++)
		if(this->name == goods[i]->name)
			number += goods[i]->count;
	return number;
}
void Good::check_saled_for_much_count(vector<Good*> goods,double Market_money){
	if( (count_of_this_good_in(goods) >= 30)
		&& ((this->price)*0.055*count_of_this_good_in(goods) <= Market_money) ){
		for(int i=0; i<goods.size(); i++)
			if(this->name == goods[i]->name){
				goods[i]->saled = true;
				goods[i]->saled_of_market = true;
			}
	}
	else{
		for(int i=0; i<goods.size(); i++)
			if(this->name == goods[i]->name){
				goods[i]->saled = false;
				goods[i]->saled_of_market = false;
				check_saled_for_requests();
			}
	}
}
bool Good::saled_position(){
	return saled;
}
string Good::return_a_line_for_factor(int number, int bought_count){
	string line="";
	line += (to_string(number)+". ");
	if(this->saled){
		line += (this->name+" | "+to_string(bought_count)+" | "+to_string(this->price*0.9)+" | ");
		line += (seller->userName()+" | "+to_string(bought_count*(this->price)*0.9)+"\n");
	}
	else{
		if(this->special_number>0){
			if(this->special_number > bought_count){
				line += (this->name+" | "+to_string(bought_count)+" | "+to_string((this->price)*0.9)+" | ");
				line += (seller->userName()+" | "+to_string(bought_count*(this->price)*0.9)+"\n");
			}
			else{
				line += (this->name+" | "+to_string(this->special_number)+" | "+to_string((this->price)*0.9)+" | ");
				line += (seller->userName()+" | "+to_string(this->special_number*(this->price)*0.9)+"\n");
				line += "\n   ";
				line += (this->name+" | "+to_string(bought_count-this->special_number)+" | "+to_string(this->price*0.9)+" | ");
				line += (seller->userName()+" | "+to_string((bought_count-this->special_number)*(this->price)*0.9)+"\n");
			}
		}
		else{
			line += (this->name+" | "+to_string(bought_count)+" | "+to_string(this->price)+" | ");
			line += (seller->userName()+" | "+to_string(bought_count*(this->price))+"\n");
		}
	}
	return line;
}
bool Good::return_saled_of_market(){
	return this->saled_of_market;
}

ostream& operator<<(ostream& out, const Good& good){
	if(good.saled){
		out << good.name << ' ' << good.count << ' ' << good.price*0.9 << ' ' << good.seller->userName();
	}
	else{
		if(good.special_number>0){
			out << good.name << ' ' << good.special_number << ' ' << good.price*0.9 << ' ' << good.seller->userName();
			out << endl << "   ";
			out << good.name << ' ' << good.count-good.special_number << ' ' << good.price << ' ' << good.seller->userName();
		}
		else{
		out << good.name << ' ' << good.count << ' ' << good.price << ' ' << good.seller->userName();
		}
	}
	return out;
}