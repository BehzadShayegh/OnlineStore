#include "market.h"

Market::Market(){
	Market_money = 0;
}

void Market::Add_user(User* user){
	if(user->exist_in(users)){
		int user_index = user->find_user_index(users);
		if(users[user_index]->ready_to_change_role(user)){
			users[user_index]->change_role_to_buyerseller(user);
			OK();
			}
		else{
			Failed();
			delete user;
			return;
		}
	}
	else{
		users.push_back(user);
		OK();
	} 
}

void Market::Add_goods(){
	string seller_username;
	cin >> seller_username;
	if(exist_in_as(seller_username,User::SELLER)){
		Good* good = new Good(users[find_user_index(seller_username)]);
		if(good->exist_in(goods) && good->same_seller(goods[good->find_good_index(goods)])){
			Failed();
			delete good;
			return;
		}
		else{
			goods.push_back(good);
			OK();
				good->check_saled_for_much_count(goods,Market_money);

			for(int i=0; i<requests.size(); i++){
				if(handle_request(requests[i])){
				delete requests[i];
				requests.erase(requests.begin()+i);
				}
			}
			return;
		}
	}
	else{
		string ignore;
		cin >> ignore >> ignore >> ignore;
		Failed();
		return;
	}
}

void Market::Search(){
	string name; int cout_need;
	cin >> name >> cout_need;
	vector<Good*> goods_to_show = find_goods(name,cout_need);
	sort_goods(goods_to_show);
	for(int i=0; i<goods_to_show.size(); i++)
		cout << i+1 << ". " << *goods_to_show[i] << endl;
}

void Market::buy(){
	string buyer_username;
	cin >> buyer_username;
	if(exist_in_as(buyer_username,User::BUYER)){
		vector<string> goods_name, seller_username;
		vector<int> count_need;
		int count_of_requests=0;
		bool again;
		string goods_name_, seller_username_;
		int count_need_;
		do{
			cin >> goods_name_ >> count_need_ >> seller_username_;
			again = (seller_username_[seller_username_.size()-1]==',');
			seller_username_.erase(seller_username_.size()-1);
				goods_name.push_back(goods_name_);
				count_need.push_back(count_need_);
				seller_username.push_back(seller_username_);	
				count_of_requests++;
		}while(again);

		double total_price = 0;
		for(int i=0; i<count_of_requests; i++){
			int good_index = find_good_index(goods_name[i],seller_username[i]);
			if(good_index<0 || !(goods[good_index]->enough_count(count_need[i])) ){
				Failed();
				return;
			}
			goods[good_index]->calculate_prices(total_price,count_need[i]);
		}
		int user_index = find_user_index(buyer_username);
		if( !(users[user_index]->enough_money(total_price)) ){
			Failed();
			return;
		}
		handle_buying(users[user_index],goods_name,count_need,seller_username,total_price);
		return;
	}
	else{
		string ignore;
		do{
			cin >> ignore;
		}while(ignore[ignore.size()-1]!=';');
		Failed();
		return;
	}
}

void Market::sale(){
	string buyer_username, goods_name, seller_username;
	cin >> buyer_username >> goods_name >> seller_username;

	if(exist_in_as(buyer_username,User::BUYER)){
		int good_index = find_good_index(goods_name,seller_username);
		if(good_index >= 0){
			if(goods[good_index]->requested_for_sale(buyer_username)){
				Failed();
				return;
			}
			else{
				goods[good_index]->add_to_sale_requesters(buyer_username);
				OK();
				return;
			}
		}
	}
	Failed();
	return;
}

void Market::add_special(){
	string seller_username, goods_name;
	int special_count_for_add;
	cin >> seller_username >> goods_name >> special_count_for_add;
	int good_index = find_good_index(goods_name,seller_username);
	if(good_index < 0){
		Failed();
		return;
	}
	else if(goods[good_index]->enough_count(special_count_for_add)){
		goods[good_index]->make_special(special_count_for_add);
		OK();
		return;
	}
	else{
		Failed();
		return;
	}
}

void Market::remove_special(){
	string seller_username, goods_name;
	int special_count_for_remove;
	cin >> seller_username >> goods_name >> special_count_for_remove;
	int good_index = find_good_index(goods_name,seller_username);
	if(good_index < 0){
		Failed();
		return;
	}
	else if(goods[good_index]->special_count() >= special_count_for_remove){
		goods[good_index]->delete_special(special_count_for_remove);
		OK();
		return;
	}
	else{
		Failed();
		return;
	}
}

void Market::search_sale(){
	vector<Good*> goods_to_show = find_saled_goods();
	sort_goods(goods_to_show);
	for(int i=0; i<goods_to_show.size(); i++)
		cout << i+1 << ". " << *goods_to_show[i] << endl;
}

void Market::search_bestseller(){
	vector<Good*> goods_to_show = find_bestseller_goods();
	sort_goods(goods_to_show);
	for(int i=0; i<goods_to_show.size(); i++)
		cout << i+1 << ". " << *goods_to_show[i] << endl;
}

void Market::add_money(){
	string username;
	double plus_money;
	cin >> username >> plus_money;
	int user_index = find_user_index(username);
	if(user_index<0){
		Failed();
		return;
	}
	else{
		users[user_index]->raise_money(plus_money);
		OK();
		return;
	}
}

void Market::request(){
	string request_name, buyer_username;
	int goods_count;
	cin >> request_name >> buyer_username;

	if(exist_in(request_name)){
		string ignore;
		do{
			cin >> ignore;
		}while(ignore[ignore.size()-1]!=';');
		Failed();
		return;
	}
	else if(exist_in_as(buyer_username,User::BUYER)){
		requests.push_back( new Request(request_name, users[find_user_index(buyer_username)]) );
		OK();
		if(handle_request(requests[requests.size()-1])){
			delete requests[requests.size()-1];
			requests.erase(requests.end());
		}
		return;
	}
	else{
		string ignore;
		do{
			cin >> ignore;
		}while(ignore[ignore.size()-1]!=';');
		Failed();
		return;
	}
}

void Market::print_request(){
	string buyer_username;
	cin >> buyer_username;
	int number_of_requests=0;
	if(exist_in_as(buyer_username,User::BUYER)){
		for(int i=0; i<requests.size(); i++)
			if(requests[i]->same_requester(buyer_username))
				requests[i]->print_this_request(++number_of_requests);
		return;
	}
	else{
		Failed();
		return;
	}
}

void Market::print_one_factor(User* buyer, vector<Good*> goods_bought, vector<int> counts, double total_price){
	vector<string> this_factor;
	this_factor.push_back( "Factor number "+to_string(int(factors.size()+1))+"\n" );
	this_factor.push_back( buyer->return_a_line_for_factor() );
	this_factor.push_back( "#. Goods Name  | Goods Count | Goods Price | Seller Username | sum (count * price )\n" );
	for(int i=0; i<goods_bought.size(); i++)
		this_factor.push_back( goods_bought[i]->return_a_line_for_factor(i+1, counts[i]) );
	this_factor.push_back( "Goods Sum = "+to_string(total_price)+"\n" );
	this_factor.push_back( "Total Sum = "+to_string(total_price*1.05)+"\n" );

	for(int i=0; i<this_factor.size(); i++)
		cout << this_factor[i];

	Factor* factor_object = new Factor(this_factor);
	factors.push_back(factor_object);
	buyer->factors_number.push_back(int(factors.size()));
}

void Market::print_factor(){
	string buyer_username;
	cin >> buyer_username;
	if(exist_in_as(buyer_username,User::BUYER)){
		int buyer_index = find_user_index(buyer_username);
		for(int i=0; i<users[buyer_index]->factors_number.size(); i++)
			factors[users[buyer_index]->factors_number[i]-1]->print_one_factor();
		return;
	}
	else{
		Failed();
		return;
	}
}

int Market::find_user_index(string _username){
	for(int i=0; i<users.size(); i++)
		if(users[i]->same_username(_username))
			return i;
	return -1;
}
bool Market::exist_in_as(string _username, User::Role _role){
	if(find_user_index(_username)<0) return false;

	User::Role role = users[find_user_index(_username)]->return_Role();
	return (role == _role || role == User::BUYER_SELLER);
}
bool Market::exist_in(string request_name){
	for(int i=0; i<requests.size(); i++)
		if(requests[i]->same_name(request_name))
			return true;
	return false;
}
int Market::find_good_index(string _name,string seller_username){
	for(int i=0; i<goods.size(); i++)
		if(goods[i]->same_name(_name) && goods[i]->use_seller()->same_username(seller_username))
			return i;
	return -1;
}
int Market::find_good_index(string _name,User* seller){
	for(int i=0; i<goods.size(); i++)
		if(goods[i]->same_name(_name) && goods[i]->use_seller()->same_username(seller))
			return i;
	return -1;
}
vector<Good*> Market::find_goods(string _name,int count_need){
	vector<Good*> ready_goods;
	for(int i=0; i<goods.size(); i++)
		if(goods[i]->there_is(_name,count_need))
			ready_goods.push_back(goods[i]);
	return ready_goods;
}
vector<Good*> Market::find_saled_goods(){
	vector<Good*> ready_goods;
	for(int i=0; i<goods.size(); i++){
		if(goods[i]->saled_position())
			ready_goods.push_back(goods[i]);
		else if(goods[i]->special_count() > 0)
			ready_goods.push_back( new Good(goods[i],goods[i]->special_count()) );
	}
	return ready_goods;
}
vector<Good*> Market::find_bestseller_goods(){
	vector<Good*> ready_goods;
	for(int i=0; i<goods.size(); i++)
		if(goods[i]->bestseller())
			ready_goods.push_back(goods[i]);
	return ready_goods;
}
void Market::sort_goods(vector<Good*>& good_s){
	for(int i=0; i<good_s.size(); i++)
		for(int j=0; j<good_s.size()-1; j++)
			if(good_s[j+1]->smaller_for_show(good_s[j])){
				Good* trash = good_s[j];
				good_s[j] = good_s[j+1];
				good_s[j+1] = trash;
			}
}

bool Market::handle_request(Request* request){
	double total_price = 0;

	User* requester = request->Get_requester();
	vector<string> goods_name = request->Get_goods_name();
	vector<int> count = request->Get_count();
	vector<string> seller_username = request->Get_seller_username();

	for(int i=0; i<count.size(); i++){
		int good_index = find_good_index(goods_name[i],seller_username[i]);
		if(good_index<0 || !(goods[good_index]->enough_count(count[i])) )
			return false;

		goods[good_index]->calculate_prices(total_price,count[i]);
	}
	if( !(requester->enough_money(total_price)) )
		return true;
	
	handle_buying(requester,goods_name,count,seller_username,total_price);
	return true;
}

void Market::handle_buying(User* requester,vector<string> goods_name,vector<int> count,vector<string> seller_username, double total_price){
	vector<Good*> bought_goods;
	for(int i=0; i<count.size(); i++){
		int good_index = find_good_index(goods_name[i],seller_username[i]);
		double transfering_money = goods[good_index]->bought(count[i]);
		goods[good_index]->check_saled_for_much_count(goods,Market_money);
		users[find_user_index(seller_username[i])]->raise_money(transfering_money);
		if(goods[good_index]->return_saled_of_market()){
			users[find_user_index(seller_username[i])]->raise_money(transfering_money*0.111111);
			Market_money -= transfering_money*0.111111;
		}
		bought_goods.push_back(goods[good_index]);
		if(goods[good_index]->is_not_avalable_in_more()){
			delete goods[good_index];
			goods.erase(goods.begin()+good_index);
		}
	}
	requester->decrease(total_price*1.05);
	Market_money += total_price*0.05;
	print_one_factor(requester,bought_goods,count,total_price);
}