#ifndef REQUEST_H
#define REQUEST_H

#include <iostream>
#include <vector>
#include "user.h"
using namespace std;

class Request{
public:
	Request(string,User*);
	bool same_name(string);
	bool same_requester(string);
	bool same_requester(User*);
	void print_this_request(int);

	User* Get_requester();
	vector<string> Get_goods_name();
	vector<int> Get_count();
	vector<string> Get_seller_username();
	
private:
	string name;
	User* requester;
	vector<string> goods_name;
	vector<int> count;
	vector<string> seller_username;
};

#endif